//
//  ContentView.swift
//  CoreMotionTest
//
//  Created by Sossio Giallaurito on 22/02/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//


import SwiftUI

struct ContentView: View {

    var body: some View {
        RecordMovementView()
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
