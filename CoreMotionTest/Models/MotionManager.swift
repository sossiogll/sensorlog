//
//  MotionManager.swift
//  CoreMotionTest
//
//  Created by Sossio Giallaurito on 22/02/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//

import Foundation
import CoreMotion


struct Motion{
    var accelerationX=0.00
    var accelerationY=0.00
    var accelerationZ=0.00
    var rotationX=0.00
    var rotationY=0.00
    var rotationZ=0.00
}

class MotionManager: ObservableObject{
    
    @Published var motion : Motion
    private let motionManager : CMMotionManager
    private let updateFrequence:Double = 10
    private var timer:Timer?
    private var csvFileContent:String?
    private var movementName : String?
    private var formatter : DateFormatter

    
    
    init(){
        
        self.formatter = DateFormatter()
        self.motionManager = CMMotionManager()
        self.motion = Motion()
        
        setupFormatter()
        initGyroscope();
        initAccelerometer();
        
            }
    
    func initGyroscope(){
        if motionManager.isGyroAvailable {
            motionManager.gyroUpdateInterval = 1/updateFrequence
            motionManager.startGyroUpdates(to: OperationQueue.main) { (g, error) in
                if let g = g {
                    var x = g.rotationRate.x
                    var y = g.rotationRate.y
                    var z = g.rotationRate.z
                                
            //                     x = round(100 * x) / 100
            //                     y = round(100 * y) / 100
            //                     z = round(100 * z) / 100

                            // Ditch the -0s because I don't like how they look being printed
                    if x.isZero && x.sign == .minus {
                        x = 0.000000
                    }

                    if y.isZero && y.sign == .minus {
                        y = 0.000000
                    }

                    if z.isZero && z.sign == .minus {
                        z = 0.000000
                    }

                    self.motion.rotationX = x
                    self.motion.rotationY = y
                    self.motion.rotationZ = z
                }
            }
        }
    }
    
    func initAccelerometer(){
        
        motionManager.deviceMotionUpdateInterval = 1/updateFrequence
        motionManager.startDeviceMotionUpdates(to: .main) { (m, error) in
        
            if let m = m {
                var x = m.userAcceleration.x
                var y = m.userAcceleration.y
                var z = m.userAcceleration.z

                            //Truncate to 2 significant digits
            //                x = round(100 * x) / 100
            //                y = round(100 * y) / 100
            //                z = round(100 * z) / 100

                            // Ditch the -0s because I don't like how they look being printed
                            
                if x.isZero && x.sign == .minus {
                    x = 0.000000
                }

                if y.isZero && y.sign == .minus {
                    y = 0.000000
                }

                if z.isZero && z.sign == .minus {
                    z = 0.000000
                }

                self.motion.accelerationX = x
                self.motion.accelerationY = y
                self.motion.accelerationZ = z
            }
        }
    }
    
    private func setupFormatter(){
        self.formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        self.formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
    }
    
    func startCollectData(MovementName:String){
        self.csvFileContent = "TimeStamp, Rotation x, Rotation y, Rotation z, Acceleration x, Aceleration y, Aceleration z, Movement Name\n"
        self.movementName = MovementName
        self.timer = Timer.scheduledTimer(timeInterval: 1/updateFrequence, target: self, selector: #selector(collectData), userInfo: nil, repeats: true)
        
    }
    
    @objc func collectData()
    {
        self.csvFileContent = (self.csvFileContent ?? "") + "\(self.formatter.string(from: NSDate() as Date)),\(self.motion.rotationX), \(self.motion.rotationY), \(self.motion.rotationZ), \(self.motion.accelerationX), \(self.motion.accelerationY), \(self.motion.accelerationZ)," +  (self.movementName ?? "") + "\n"
    }
    
    func stopCollectData(){
        timer?.invalidate()
    }
    
    func exportData()->String{

        let fileManager = FileManager.default
        do {
            let path = try fileManager.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
            let fileURL = path.appendingPathComponent((self.movementName ?? "motionData")+".csv")
            try self.csvFileContent?.write(to: fileURL, atomically: true, encoding: .utf8)
            return fileURL.absoluteString
        } catch {
            print("error creating file")
            abort()
        }

    }


}

