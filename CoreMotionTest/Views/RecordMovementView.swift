//
//  RecordMovement.swift
//  CoreMotionTest
//
//  Created by Sossio Giallaurito on 26/02/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//

import SwiftUI

struct RecordMovementView: View {
    @EnvironmentObject var motionManager: MotionManager
    @State private var movementName: String = ""
    @State private var buttonState: String = "Registra movimento"
    @State private var isRecording = false
    @State private var isSharePresented: Bool = false
    @State private var filePath = ""

    var body: some View {
        VStack{
               
            Spacer()

                Text("Accelerazione")
                    .bold()
                    .font(.title)
                
                HStack{
                    Spacer()
                    Text("x: \(motionManager.motion.accelerationX)")
                    Spacer()
                    Text("y: \(motionManager.motion.accelerationY)")
                    Spacer()
                    Text("z: \(motionManager.motion.accelerationZ)")
                    Spacer()
                }
            
                Text("Rotazione")
                    .bold()
                    .font(.title)
            
               HStack{
                    Spacer()
                    Text("x: \(motionManager.motion.rotationX)")
                    Spacer()
                    Text("y: \(motionManager.motion.rotationY)")
                    Spacer()
                    Text("z: \(motionManager.motion.rotationZ)")
                    Spacer()
               }
               
                Spacer()
                TextField("Inserisci nome movimento", text: $movementName,
                        onEditingChanged: { (changed) in
                        print("onEditingChanged - \(changed)")
                    }) {
                        print("onCommit")
                    }
                        .padding(.all)
                        .border(!isRecording ? Color.blue : Color.gray)
                        .disabled(isRecording)

               
               
                Button(action: {
                    if self.isRecording{
                        self.buttonState = "Registra movimento"
                        self.isRecording = false
                        self.isSharePresented = true
                        self.motionManager.stopCollectData();
                        self.filePath = self.motionManager.exportData();
                   }
                   else{
                        self.buttonState = "Ferma registrazione"
                        self.isRecording = true
                        self.isSharePresented = false
                        print("registra movimento")
                        self.motionManager.startCollectData(MovementName: self.movementName);

                   }
               }
               ){
                   Text(buttonState)
                   .padding()
                       .foregroundColor(Color.white)
                       .background(!isRecording ? Color.blue : Color.red)
                   .cornerRadius(10)
               }
                    .sheet(isPresented: $isSharePresented) {
                        ActivityViewController(FilePath: self.filePath)
                    }
            
               

               Spacer()
            Spacer()
            

           }
       }
}


struct RecordMovement_Previews: PreviewProvider {
    static var previews: some View {
        RecordMovementView()
    }
}

