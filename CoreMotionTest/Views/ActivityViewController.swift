//
//  ActivityViewController.swift
//  CoreMotionTest
//
//  Created by Sossio Giallaurito on 26/02/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//

import SwiftUI
import UIKit

struct ActivityViewController: UIViewControllerRepresentable {
    
    private var filePath = "";
    private var applicationActivities: [UIActivity]? = nil
    private var firstInit = true
    init(FilePath: String){
        if firstInit{
            self.filePath = FilePath
            firstInit = false
            print("Primo init \(self.filePath)")
        }
    }
    

    func makeUIViewController(context: UIViewControllerRepresentableContext<ActivityViewController>) -> UIActivityViewController {
        print("Nome file ricevuto: .\(filePath)")

        let activityItem = [NSURL(fileURLWithPath: self.filePath)]
        
        let controller = UIActivityViewController(activityItems: activityItem, applicationActivities: applicationActivities)
        
        controller.excludedActivityTypes = [
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        return controller
    }

    func updateUIViewController(_ uiViewController: UIActivityViewController, context: UIViewControllerRepresentableContext<ActivityViewController>) {}
    

}
